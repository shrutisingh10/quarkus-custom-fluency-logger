# Quarkus fluency-fluentd log handler project

Quarkus log-handler extension using fluency-fluentd to handle logs.

## EFK

Run efk on docker use below command inside efk-docker directory: 
```shell script
docker-compose up -d
```

This will start fluentd on localhost:24224.

## Configuration

The extension is not enabled by default, when added to the project. Can be enabled using configuration: ```quarkus.custom.log.handler.enable=true```

## Add additional fields

If you want to add a specific host, that is possible using the configuration.
```shell script
quarkus.custom.log.handler.host=localhost
quarkus.custom.log.handler.port=24224
```

