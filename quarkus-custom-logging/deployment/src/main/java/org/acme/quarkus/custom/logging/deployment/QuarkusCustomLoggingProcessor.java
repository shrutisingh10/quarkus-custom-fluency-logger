package org.acme.quarkus.custom.logging.deployment;

import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.annotations.ExecutionTime;
import io.quarkus.deployment.annotations.Record;
import io.quarkus.deployment.builditem.FeatureBuildItem;
import io.quarkus.deployment.builditem.LogHandlerBuildItem;
import org.acme.quarkus.custom.logging.CustomLogConfig;
import org.acme.quarkus.custom.logging.CustomLogRecorder;

import java.io.IOException;

class QuarkusCustomLoggingProcessor {

    private static final String FEATURE = "quarkus-custom-logging";

    @BuildStep
    FeatureBuildItem feature() {
        return new FeatureBuildItem(FEATURE);
    }

    @BuildStep
    @Record(ExecutionTime.RUNTIME_INIT)
    LogHandlerBuildItem build(CustomLogRecorder recorder, CustomLogConfig config) throws IOException {
        return new LogHandlerBuildItem(recorder.initializeHandler(config));
    }
}
