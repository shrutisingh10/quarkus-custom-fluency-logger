package org.acme.quarkus.custom.logging;

import io.quarkus.runtime.annotations.ConfigItem;
import io.quarkus.runtime.annotations.ConfigPhase;
import io.quarkus.runtime.annotations.ConfigRoot;

@ConfigRoot(phase = ConfigPhase.RUN_TIME, name = "custom.log.handler")
public class CustomLogConfig {
    /**
     * Determine whether to enable the custom logging handler
     */
    @ConfigItem
    public boolean enabled;

    /**
     * Hostname/IP-Address of the fluentd Host
     * By default it uses "localhost"
     */
    @ConfigItem(defaultValue = "localhost")
    public String host;

    /**
     * The port of fluentd
     */
    @ConfigItem(defaultValue = "24224")
    public int port;

}
