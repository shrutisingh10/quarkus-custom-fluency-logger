package org.acme.quarkus.custom.logging;

import io.quarkus.runtime.RuntimeValue;
import io.quarkus.runtime.annotations.Recorder;

import java.util.Optional;
import java.util.logging.Handler;

@Recorder
public class CustomLogRecorder {

    public RuntimeValue<Optional<Handler>> initializeHandler(final CustomLogConfig config){
        if (!config.enabled) {
            return new RuntimeValue<>(Optional.empty());
        }
        final CustomLogHandler customLogHandler = new CustomLogHandler();
        customLogHandler.setPort(config.port);
        customLogHandler.setHost(config.host);
        return new RuntimeValue<>(Optional.of(customLogHandler));
    }
}
