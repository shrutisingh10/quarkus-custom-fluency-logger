package org.acme.quarkus.custom.logging;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.komamitsu.fluency.Fluency;
import org.komamitsu.fluency.fluentd.FluencyBuilderForFluentd;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class CustomLogHandler extends Handler {

  private int port;
  private String host;
  private Fluency fluency;
  private final ObjectMapper objectMapper = new ObjectMapper();

  public void setPort(int port) {
    this.port = port;
  }

  public void setHost(String host) {
    this.host = host;
  }

  @Override
  public void publish(LogRecord record) {
    if (fluency == null) {
      fluency = connect();
    }
    Map<String, Object> event = objectMapper.convertValue(record, Map.class);
    try {
      fluency.emit("application-logs", event);
    } catch (IOException e) {
      this.reportError("Could not send logs" + e.getMessage(), e, 4);
      e.printStackTrace();
    }
  }

  private Fluency connect() {
    return new FluencyBuilderForFluentd().build(host, port);
  }

  @Override
  public void flush() {
    try {
      fluency.flush();
    } catch (IOException e) {
      this.reportError(e.getMessage(), e, 4);
      e.printStackTrace();
    }
  }

  @Override
  public void close() throws SecurityException {
    try {
      fluency.close();
    } catch (IOException e) {
      this.reportError(e.getMessage(), e, 4);
      e.printStackTrace();
    }
  }
}
